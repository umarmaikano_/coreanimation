//
//  Item.swift
//  CoreAnimation
//
//  Created by Umar Maikano on 29/01/2021.
//

import Foundation

enum Item {
    case header
    case moreItem
    case emptyField
    case content(ContentType)
}

enum ContentType {
    case player(Player)
    case team(Team)
}

struct Team {
    let name: String
    let city: String
    let stadium: String
    let nationality: String
}

struct Player {
    let name : String
    let nationality : String
    let age : String
    let club : String
}
