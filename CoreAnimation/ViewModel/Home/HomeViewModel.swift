//
//  HomeViewModel.swift
//  CoreAnimation
//
//  Created by Umar Maikano on 29/01/2021.
//

import Foundation

class HomeViewModel: HomeViewModelType {
    weak var delegate: HomeViewControllerDelegate?
    let networkManager: HomeNetworkManagerType!
    let coreDataManager: HomeCoreDataManagerType!
    var items: [Item] = []
    var timer: Timer?
    var searchString: String? {
        didSet {
            startTimer()
        }
    }
    
    init(delegate: HomeViewControllerDelegate) {
        self.delegate = delegate
        networkManager = HomeNetworkManager()
        coreDataManager = HomeCoreDataManager()
    }
    
    func arrangeItems(with players: [Player], and items: [Item]) -> [Item] {
        var newPlayerItems: [Item] = []
        var items = items
        
        for player in players {
            newPlayerItems.append(.content(.player(player)))
        }
        
        if let index = items.lastIndex(where: { if case .content(.player) = $0 { return true }; return false }) {
            items.insert(contentsOf: newPlayerItems, at: index)
        }
        
        return items
    }
    
    func arrangeItems(with teams: [Team], and items: [Item]) -> [Item] {
        var newTeamItems: [Item] = []
        var items = items
        
        for team in teams {
            newTeamItems.append(.content(.team(team)))
        }
        
        if let index = items.lastIndex(where: { if case .content(.team) = $0 { return true }; return false }) {
            items.insert(contentsOf: newTeamItems, at: index)
        }
        
        return items
    }
    
    func arrangeItems(with players: [Player], and teams: [Team]) -> [Item] {
        var items: [Item] = []
        items.append(.header)
        
        for player in players {
            items.append(.content(.player(player)))
        }
        
        items.append(.moreItem)
        
        items.append(.header)
        
        for team in teams {
            items.append(.content(.team(team)))
        }
        
        items.append(.moreItem)
        return items
    }
    
    func startTimer() {
        if timer == nil  {
            timer?.invalidate()
        }
        
        timer = Timer(timeInterval: 0.25, repeats: true, block: { timer in
            self.getItems()
            timer.invalidate()
        })
        
        timer?.fire()
    }
    
    func morePlayers() {
        getPlayers()
    }
    
    func moreTeams() {
        getTeams()
    }
    
    func fetchItem(with searchString: String) {
        self.searchString = searchString
    }
    
    private func getTeams() {
        guard let searchString = searchString else {
            return
        }
        
        delegate?.setLoadingState()
        
        networkManager.fetchTeams(with: searchString, completion: { result in
            
            switch result {
            case .success(let teamResult):
                self.items = self.arrangeItems(with: teamResult.getTeams(), and: self.items)
                self.delegate?.renderItem(items: self.items)
            case .failure(_):
                self.delegate?.setErrorState()
            }
        })
    }
    
    private func getPlayers() {
        guard let searchString = searchString else {
            return
        }
        
        delegate?.setLoadingState()
        
        networkManager.fetchPlayers(with: searchString, completion: { result in
            
            switch result {
            case .success(let playerResult):
                self.items = self.arrangeItems(with: playerResult.getPlayers(), and: self.items)
                self.delegate?.renderItem(items: self.items)
            case .failure(_):
                self.delegate?.setErrorState()
            }
        })
    }
    
    private func getItems() {
        guard let searchString = searchString else {
            return
        }
        delegate?.setLoadingState()
        
        networkManager.fetchItems(with: searchString) { [self] result in
            switch result {
            case .success(let itemResults):
                self.items = arrangeItems(with: itemResults.getPlayers(), and: itemResults.getTeams())
                delegate?.renderItem(items: self.items)
            case .failure(_):
                self.delegate?.setErrorState()
            }
        }
    }
}
