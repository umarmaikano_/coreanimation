//
//  CoreDataManager.swift
//  CoreAnimation
//
//  Created by Umar Maikano on 29/01/2021.
//

import Foundation

protocol CoreDataManagerType {
    func savePlayer()
    func saveTeam()
    func deleteTeam()
    func deletePlayer()
    func getSavedItems()
}

class CoreDataManager: CoreDataManagerType {
    static let shared = CoreDataManager()
    
    func saveTeam() {
        
    }
    
    func deleteTeam() {
        
    }
    
    func deletePlayer() {
        
    }
    
    func getSavedItems() {
        
    }
    
    func savePlayer() {
        
    }
}
