//
//  HomeCoreDataManager.swift
//  CoreAnimation
//
//  Created by Umar Maikano on 29/01/2021.
//

import Foundation

protocol HomeCoreDataManagerType {
    func saveTeam()
    func savePlayer()
}

class HomeCoreDataManager: HomeCoreDataManagerType {
    func saveTeam() {
        CoreDataManager.shared.saveTeam()
    }
    
    func savePlayer() {
        CoreDataManager.shared.savePlayer()
    }
}
