//
//  HomeNetworkManagerProtocol.swift
//  CoreAnimation
//
//  Created by Umar Maikano on 29/01/2021.
//

import Foundation

protocol HomeNetworkManagerType {
    func fetchItems(with searchString: String, completion: @escaping (Result<ItemResponse, Error>) -> Void)
    func fetchPlayers(with searchString: String, completion: @escaping (Result<PlayerResult, Error>) -> Void)
    func fetchTeams(with searchString: String, completion: @escaping (Result<TeamResult, Error>) -> Void)
}
