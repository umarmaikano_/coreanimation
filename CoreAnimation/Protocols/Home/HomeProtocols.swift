//
//  HomeProtocols.swift
//  CoreAnimation
//
//  Created by Umar Maikano on 29/01/2021.
//

import Foundation

protocol HomeViewModelType {
    func fetchItem(with searchString: String)
    func morePlayers()
    func moreTeams()
}

protocol HomeViewControllerDelegate: AnyObject {
    func renderItem(items: [Item])
    func setLoadingState()
    func setErrorState()
}
