//
//  ViewController.swift
//  CoreAnimation
//
//  Created by Umar Maikano on 29/01/2021.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var itemsTableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    
    var viewModel: HomeViewModelType!
    var items: [Item] = [] {
        didSet {
            DispatchQueue.main.async {
                self.itemsTableView.isHidden = self.items.isEmpty
            }
            reload()
        }
    }
    var markedItems: [ContentType] = []
    var state: State? {
        didSet {
            switch state {
            case .loading:
                showLoadingView()
            default:
                hideLoadingView()
            }
        }
    }
    
    //loading view
    var loadingView: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = .large
        indicator.color = .blue
        
        // The indicator should be animating when
        // the view appears.
        indicator.startAnimating()
        
        // Setting the autoresizing mask to flexible for all
        // directions will keep the indicator in the center
        // of the view and properly handle rotation.
        indicator.autoresizingMask = [
            .flexibleLeftMargin, .flexibleRightMargin,
            .flexibleTopMargin, .flexibleBottomMargin
        ]
        
        indicator.isHidden = true
        
        return indicator
    }()
    
    enum State {
        case loading
        case error
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        viewModel = HomeViewModel(delegate: self)
        setUpDismissKeyboard()
        setUploadingView()
        registerTableCells()
    }
    
    func registerTableCells() {
        
    }
    
    func setUploadingView() {
        // Add the loadingActivityIndicator in the
        // center of view
        loadingView.center = CGPoint(
            x: view.bounds.midX,
            y: view.bounds.midY
        )
        
        // add to view
        view.addSubview(loadingView)
    }
    
    // Dismiss keyboard
    func setUpDismissKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissKeyboard))

        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func reload() {
        DispatchQueue.main.async {
            self.itemsTableView.reloadData()
        }
    }
    
    private func showLoadingView() {
        loadingView.isHidden = false
    }
    
    private func hideLoadingView() {
        DispatchQueue.main.async { [self] in
            self.loadingView.isHidden = true
        }
    }

}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = items[indexPath.item]
        
        switch item {
        case .header: break
        case .emptyField: break
        case .moreItem: break
        case .content(let type):
            switch type {
            case .player(_): break
            case .team(_): break
            }
        }
        
        return UITableViewCell()
    }
}

extension HomeViewController: HomeViewControllerDelegate {
    func setLoadingState() {
        self.state = .loading
    }
    
    func setErrorState() {
        self.state = .error
    }
    func renderItem(items: [Item]) {
        self.items = items
        self.state = nil
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = items[indexPath.item]
        
        switch item {
        case .header, .emptyField, .moreItem: break
        case .content(let type):
            switch type {
            case .player(_): break
            case .team(_): break
            }
        }
        
        return 44
    }
}

extension HomeViewController: UITextFieldDelegate {
    func textFieldDidChangeSelection(_ textField: UITextField) {
        guard  textField == searchTextField, let searchString = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines), !searchString.isEmpty  else {
            return
        }
        
        viewModel.fetchItem(with: searchString)
    }
}

extension HomeViewController: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}
