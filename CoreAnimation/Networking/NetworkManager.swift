//
//  NetworkManager.swift
//  CoreAnimation
//
//  Created by Umar Maikano on 29/01/2021.
//

import Foundation

enum ErrorType: String, Error {
    case networkingError
    case parsingError
    case noData
    case responseError
}

class NetworkManager {
    
    static let shared = NetworkManager() // network manager singelton

    func getRequest<T: Decodable>(ofType: T.Type, with url: URL, completion: @escaping (Result<T, Error>) -> Void) {
        
        let session = URLSession.shared.dataTask(with: url, completionHandler: { data,response,error in
            
            guard error == nil else {
                completion(.failure(ErrorType.networkingError))
                return
            }
            
            guard let response = response as? HTTPURLResponse,  response.statusCode == 200 else {
                completion(.failure(ErrorType.responseError))
                return
            }
            
            guard let data = data else {
                completion(.failure(ErrorType.noData))
                return
            }
            
            do {
                let result = try JSONDecoder.init().decode(T.self, from: data)
                completion(.success(result))
            } catch(_) {
                completion(.failure(ErrorType.parsingError))
            }
            
        })
        
        session.resume()
    }
    
    // decoder model
    private func decodeData<T: Decodable>(ofType: T.Type, data: Data) -> T? {
        let decoder = JSONDecoder.init()
        do {
            let result = try decoder.decode(ofType, from: data)
            return result
        }
        catch(_) {
            return nil
        }
    }
}
