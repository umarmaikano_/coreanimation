//
//  HomeNetworkManager.swift
//  CoreAnimation
//
//  Created by Umar Maikano on 29/01/2021.
//

import Foundation


struct TeamResult: Decodable {
    let result: TeamResults
}


// MARK: - TeamResult
struct TeamResults: Decodable {
    let teams: [TeamResponse]?
    let status: Bool
    let message: String
    let requestOrder: Int
    let searchType, searchString, minVer, serverAlert: String

    enum CodingKeys: String, CodingKey {
        case teams, status, message
        case requestOrder = "request_order"
        case searchType, searchString, minVer, serverAlert
    }
}

// MARK: - Team
struct TeamResponse: Decodable {
    let teamID, teamName, teamStadium, isNation: String
    let teamNationality, teamCity: String
}


extension TeamResult {
    func getTeams() -> [Team] {
        return []
    }
}

struct PlayerResult: Decodable {
    let result: PlayerResults
}

// MARK: - PlayerResult
struct PlayerResults: Decodable {
    let players : [PlayerResponse]?
    let status : Bool
    let message : String
    let request_order : Int
    let searchType : String
    let searchString : String
    let minVer : String
    let serverAlert : String
}

// MARK: - Players
struct PlayerResponse: Decodable {
    let playerID : String
    let playerFirstName : String
    let playerSecondName : String
    let playerNationality : String
    let playerAge : String
    let playerClub : String
}


extension PlayerResult {
    func getPlayers() -> [Player] {
        return []
    }
}


//MARK: - Search result model for both player and team
struct ItemResponse: Decodable {
    let result: Results
}

struct Results : Decodable {
    let players : [PlayerResponse]?
    let teams : [TeamResponse]?
    let status : Bool?
    let message : String?
    let request_order : Int?
    let searchType : String?
    let searchString : String?
    let minVer : String?
    let serverAlert : String?
}
extension ItemResponse {
    func getPlayers() -> [Player] {
        guard let playerResults = result.players else {
            return []
        }
        
        return playerResults.map { (player) -> Player in
            return .init(name: player.playerFirstName, nationality: player.playerNationality, age: player.playerAge, club: player.playerClub )
        }
    }
    
    func getTeams() -> [Team] {
        guard let teamResults = result.teams else {
            return []
        }
        
        return teamResults.map {(team) -> Team in
            return .init(name: team.teamName, city: team.teamCity, stadium: team.teamStadium, nationality: team.teamNationality)
        }
    }
}


class HomeNetworkManager: HomeNetworkManagerType {
    var urlComponents = URLComponents(string: "https://trials.mtcmobile.co.uk/api/football/1.0/search")
    
    func fetchItems(with searchString: String, completion: @escaping (Result<ItemResponse, Error>) -> Void) {
        let queryItem: [URLQueryItem] = [URLQueryItem(name: "searchString", value: searchString), URLQueryItem(name: "requestOrder", value: "")]
        urlComponents?.queryItems = queryItem
        
        guard let url = urlComponents?.url else {
            return
        }
        
        NetworkManager.shared.getRequest(ofType: ItemResponse.self, with: url, completion: { result in
            switch result {
            case .success(let result):
                completion(.success(result))
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
    
    func fetchPlayers(with searchString: String, completion: @escaping (Result<PlayerResult, Error>) -> Void) {
        let queryItem: [URLQueryItem] = [URLQueryItem(name: "searchString", value: searchString), URLQueryItem(name: "", value: "")]
        urlComponents?.queryItems = queryItem
        
        guard let url = urlComponents?.url else {
            return
        }
        
        NetworkManager.shared.getRequest(ofType: PlayerResult.self, with: url, completion: { result in
            switch result {
            case .success(let result):
                completion(.success(result))
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
    
    func fetchTeams(with searchString: String, completion: @escaping (Result<TeamResult, Error>) -> Void) {
        let queryItem: [URLQueryItem] = [URLQueryItem(name: "searchString", value: searchString), URLQueryItem(name: "offset", value: "")]
        urlComponents?.queryItems = queryItem
        
        guard let url = urlComponents?.url else {
            return
        }
        
        NetworkManager.shared.getRequest(ofType: TeamResult.self, with: url, completion: { result in
            switch result {
            case .success(let result):
                completion(.success(result))
            case .failure(let error):
                completion(.failure(error))
            }
            
        })
    }
}
